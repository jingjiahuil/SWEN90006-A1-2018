package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
	 @Before public void setUp()
	  {
	  }

	  //Any method annotated with "@After" will be executed after each test,
	  //allowing the tester to release any shared resources used in the setup.
	  @After public void tearDown()
	  {
	  }

	  //Any method annotation with "@Test" is executed as a test.

	  @Test public void EC1B1()
	  {
		String a="RET R0;";
	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    Machine m = new Machine();
	    assertEquals(m.execute(list), 0);
	  }
	  
	  @Test public void EC1B2()
	  {
		String a="RET R31;";
	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    Machine m = new Machine();
	    assertEquals(m.execute(list), 0);
	  }

	  @Test(expected=InvalidInstructionException.class)
	  public void EC2B1() 
		  throws Throwable
		  {
		    String a="RET R33;";
		    final List<String> list = new ArrayList<String>();
		    list.add(a);
		    Machine machine = new Machine();
		    assertEquals("Invalid",machine.execute(list),InvalidInstructionException.class);
		  }
	  

	  @Test public void EC5B1()
	  {
		String a="MOV R2 1;";
		String b="MOV R1 2;";
	    String c="ADD R0 R1 R2;";
	    String d="RET R0;";
	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    list.add(b);
	    list.add(c);
	    list.add(d);
	    Machine m = new Machine();
	    assertEquals(m.execute(list), 3);
	  }
	  @Test public void EC5B2()
	  {
		String a="MOV R2 1;";
		String b="MOV R1 2;";
	    String c="ADD R31 R1 R2;";
	    String d="RET R31;";
	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    list.add(b);
	    list.add(c);
	    list.add(d);
	    Machine m = new Machine();
	    assertEquals(m.execute(list), 3);
	  }
	  @Test(expected=InvalidInstructionException.class) 
	  public void EC6B1()
	  {
		String a="MOV R0 1;";
		String b="MOV R1 2;";
	    String c="ADD R32 R1 R2;";
	    String d="RET R32;";
	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    list.add(b);
	    list.add(c);
	    list.add(d);
	    Machine m = new Machine();
	    assertEquals("Invalid",m.execute(list),InvalidInstructionException.class);
	  }
	  
	  @Test public void EC7()
	  {
		String a="MOV R1 6;";
		String b="MOV R2 0;";
	    String d="DIV R3 R1 R2;";
		String e="RET R3;";
	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    list.add(b);
	    list.add(d);
	    list.add(e);
	    Machine m = new Machine();
	    assertEquals(m.execute(list), 0);
	  }
	  @Test public void EC8()
	  {
		String a="MOV R1 6;";
		String b="MOV R2 2;";
	    String c="MOV R3 6;";
	    String d="DIV R3 R1 R2;";
	    String e="RET R3;";
	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    list.add(b);
	    list.add(c);
	    list.add(d);
	    list.add(e);
	    Machine m = new Machine();
	    assertEquals(m.execute(list), 3);
	  }
	  
	  @Test public void EC9B1()
	  {
		String a="MOV R3 65535;";
		String b="RET R3;";
	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    list.add(b);
	    Machine m = new Machine();
	    assertEquals(m.execute(list), 65535);
	  }
	  
	  @Test public void EC9B2()
	  {
		String a="MOV R3 -65535;";
		String b="RET R3;";
	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    list.add(b);
	    Machine m = new Machine();
	    assertEquals(m.execute(list), -65535);
	  }
	  
	  @Test(expected=InvalidInstructionException.class)
	    public void EC10B1()
	  {
		String a="MOV R3 -65536";
		String b="RET R3;";
	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    list.add(b);
	    Machine m = new Machine();
	    assertEquals("Invalid",m.execute(list),InvalidInstructionException.class);
	  }
	  
	  @Test(expected=InvalidInstructionException.class) 
	  public void EC10B2()
	  {
		String a="MOV R3 65536";
		String b="RET R3;";
	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    list.add(b);
	    Machine m = new Machine();
	    assertEquals("Invalid",m.execute(list),InvalidInstructionException.class);
	  }
	  
	  @Test(expected=InvalidInstructionException.class)
	  
	  
	  public void EC12B1()
	  {
		    String a="MOV R1 0;";
		    String b="MOV R2 7;";
		    String c="LDR R2 R1 65536;";
		    String d="RET R2;";

		    final List<String> list = new ArrayList<String>();
		    list.add(a);
		    list.add(b);
		    list.add(c);
		    list.add(d);
		    Machine m = new Machine();
		    assertEquals("Invalid",m.execute(list),InvalidInstructionException.class);
	  }
	  
	  @Test(expected=InvalidInstructionException.class)
	  public void EC12B2()
	  {
		    String a="MOV R1 0;";
		    String b="MOV R2 7;";
		    String c="LDR R2 R1 -65536;";
		    String d="RET R2;";

		    final List<String> list = new ArrayList<String>();
		    list.add(a);
		    list.add(b);
		    list.add(c);
		    list.add(d);
		    Machine m = new Machine();
		    assertEquals("Invalid",m.execute(list),InvalidInstructionException.class);
	  }
	  @Test(expected=NoReturnValueException.class)
	   public void EC14B1()
	   {
	 	    String b="JMP 65535;";
	 	    final List<String> list = new ArrayList<String>();
	 	    list.add(b);
	 	    Machine m = new Machine();
	 	    assertEquals("Invalid",m.execute(list),NoReturnValueException.class);
	   }
	   
	  @Test(expected=NoReturnValueException.class)
	   public void EC14B2()
	   {
	 	    String b="JMP -65535;";
	 	    final List<String> list = new ArrayList<String>();
	 	    list.add(b);
	 	    Machine m = new Machine();
	 	    assertEquals("Invalid",m.execute(list),NoReturnValueException.class);
	   }
	   
	  
	  @Test(expected=InvalidInstructionException.class)
	  public void EC15B1()
	  {
		    String b="JMP -65536;";
		    final List<String> list = new ArrayList<String>();
		    list.add(b);
		    Machine m = new Machine();
		    assertEquals("Invalid",m.execute(list),InvalidInstructionException.class);
	  }
	  
	  @Test(expected=InvalidInstructionException.class)
	  public void EC15B2()
	  {
		    String b="JMP 65536;";
		    final List<String> list = new ArrayList<String>();
		    list.add(b);
		    Machine m = new Machine();
		    assertEquals("Invalid",m.execute(list),InvalidInstructionException.class);
	  }

	  /*
	   * [EC16]: {list|length>1/\INSTUCTION=LDR/\regsi in [R0,R1,...,R31]/\vali in[-65536,65535]/\reg[a]+offs>65535} invalid
	MOV R1 1; MOV R2 7; LDR R2 R1 65535; RET R2;
	   */
	  
	  @Test(expected=InvalidInstructionException.class)
	  public void EC16()
	  {
		    String a="MOV R1 1;";
		    String b="MOV R2 7;";
		    String c="LDR R2 R1 65535;";
		    String d="RET 0;";
		    final List<String> list = new ArrayList<String>();
		    list.add(a);
		    list.add(b);
		    list.add(c);
		    list.add(d);
		    Machine m = new Machine();
		    assertEquals("Invalid",m.execute(list),InvalidInstructionException.class);
	  }
	  
	  
	  @Test
	  public void EC17B1()
	  {
		    String a="MOV R1 1;";
		    String b="MOV R2 7;";
		    String c="LDR R2 R1 65534;";
		    String d="RET R2;";
		    final List<String> list = new ArrayList<String>();
		    list.add(a);
		    list.add(b);
		    list.add(c);
		    list.add(d);
		    Machine m = new Machine();
		    assertEquals(m.execute(list),0);
	  }
	  
	  @Test
	  public void EC17B2()
	  {
		    String a="MOV R1 0;";
		    String b="MOV R2 7;";
		    String c="LDR R2 R1 0;";
		    String d="RET R2;";
		    final List<String> list = new ArrayList<String>();
		    list.add(a);
		    list.add(b);
		    list.add(c);
		    list.add(d);
		    Machine m = new Machine();
		    assertEquals(m.execute(list),0);
	  }

	  @Test
	  public void EC18()
	  {
		    String a="MOV R1 -1;";
		    String b="MOV R2 7;";
		    String c="LDR R2 R1 0;";
		    String d="RET R2;";
		    final List<String> list = new ArrayList<String>();
		    list.add(a);
		    list.add(b);
		    list.add(c);
		    list.add(d);
		    Machine m = new Machine();
		    assertEquals(m.execute(list),7);
	  }
	  

}
