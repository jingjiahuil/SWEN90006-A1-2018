package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class PartitioningTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //Any method annotation with "@Test" is executed as a test.

  @Test public void EC1()
  {
	String a="RET R2;";
    final List<String> list = new ArrayList<String>();
    list.add(a);
    Machine m = new Machine();
    assertEquals(m.execute(list), 0);
  }

  @Test(expected=InvalidInstructionException.class)
  public void EC2() 
	  throws Throwable
	  {
	    String a="RET R33;";
	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    Machine machine = new Machine();
	    assertEquals("Invalid",machine.execute(list),InvalidInstructionException.class);
	  }
  
  @Test(expected=NoReturnValueException.class) 
  public void EC3()
  {
	String a="MOV R0 1;";
    final List<String> list = new ArrayList<String>();
    list.add(a);

    Machine m = new Machine();
    assertEquals("Invalid",m.execute(list),NoReturnValueException.class);
  }

  @Test public void EC4()
  {
	String a="MOV R2 1;";
	String b="MOV R1 2;";
    String c="ADD R3 R1 R2;";
    String d="RET R3;";
    final List<String> list = new ArrayList<String>();
    list.add(a);
    list.add(b);
    list.add(c);
    list.add(d);
    Machine m = new Machine();
    assertEquals(m.execute(list), 3);
  }

  @Test(expected=InvalidInstructionException.class) 
  public void EC5()
  {
	String a="MOV R0 1;";
	String b="MOV R1 2;";
    String c="ADD R35 R1 R2;";
    String d="RET R35;";
    final List<String> list = new ArrayList<String>();
    list.add(a);
    list.add(b);
    list.add(c);
    list.add(d);
    Machine m = new Machine();
    assertEquals("Invalid",m.execute(list),InvalidInstructionException.class);
  }

  @Test public void EC6()
  {
	String a="MOV R1 6;";
	String b="MOV R2 2;";
    String c="MOV R3 6;";
    String d="DIV R3 R1 R2;";
    String e="RET R3;";
    final List<String> list = new ArrayList<String>();
    list.add(a);
    list.add(b);
    list.add(c);
    list.add(d);
    list.add(e);
    Machine m = new Machine();
    assertEquals(m.execute(list), 3);
  }
  
  @Test public void EC7()
  {
	String a="MOV R1 6;";
	String b="MOV R2 0;";
    String d="DIV R3 R1 R2;";
	String e="RET R3;";
    final List<String> list = new ArrayList<String>();
    list.add(a);
    list.add(b);
    list.add(d);
    list.add(e);
    Machine m = new Machine();
    assertEquals(m.execute(list), 0);
  }
  
  @Test public void EC8()
  {
	String a="MOV R3 65535;";
	String b="RET R3;";
    final List<String> list = new ArrayList<String>();
    list.add(a);
    list.add(b);
    Machine m = new Machine();
    assertEquals(m.execute(list), 65535);
  }
  
  /*
   * [EC10]: {list|length>1/\INSTUCTION in [MOV,JZ]/\regsi in [R0,R1,...,R31]/\vali not in[-65536,65535]} invalid
MOV R3 65536; RET R3;
   */
  @Test(expected=InvalidInstructionException.class)
    public void EC9()
  {
	String a="MOV R3 65536";
	String b="RET R3;";
    final List<String> list = new ArrayList<String>();
    list.add(a);
    list.add(b);
    Machine m = new Machine();
    assertEquals("Invalid",m.execute(list),InvalidInstructionException.class);
  } 
  
  
  @Test(expected=NoReturnValueException.class)
   public void EC10()
   {
 	    String b="JMP 65535;";
 	    final List<String> list = new ArrayList<String>();
 	    list.add(b);
 	    Machine m = new Machine();
 	    assertEquals("Invalid",m.execute(list),NoReturnValueException.class);
   }
   
  
  @Test(expected=InvalidInstructionException.class)
  public void EC11()
  {
	    String b="JMP -65536;";
	    final List<String> list = new ArrayList<String>();
	    list.add(b);
	    Machine m = new Machine();
	    assertEquals("Invalid",m.execute(list),InvalidInstructionException.class);
  }
  
  @Test(expected=InvalidInstructionException.class)
  public void EC12()
  {
	    String a="MOV R1 0;";
	    String b="MOV R2 7;";
	    String c="LDR R2 R1 65536;";
	    String d="RET R2;";

	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    list.add(b);
	    list.add(c);
	    list.add(d);
	    Machine m = new Machine();
	    assertEquals("Invalid",m.execute(list),InvalidInstructionException.class);
  }
  @Test
  public void EC13()
  {
	    String a="MOV R1 2;";
	    String b="MOV R2 7;";
	    String c="LDR R2 R1 65534;";
	    String d="RET R2;";

	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    list.add(b);
	    list.add(c);
	    list.add(d);
	    Machine m = new Machine();
	    assertEquals(m.execute(list),7);
  }
  
  
  @Test
  public void EC14()
  {
	    String a="MOV R1 55;";
	    String b="MOV R2 7;";
	    String c="LDR R2 R1 -44;";
	    String d="RET R2;";
	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    list.add(b);
	    list.add(c);
	    list.add(d);
	    Machine m = new Machine();
	    assertEquals(m.execute(list),0);
  }
  
  @Test
  public void EC15()
  {
	    String a="MOV R1 -1;";
	    String b="MOV R2 7;";
	    String c="LDR R2 R1 0;";
	    String d="RET R2;";
	    final List<String> list = new ArrayList<String>();
	    list.add(a);
	    list.add(b);
	    list.add(c);
	    list.add(d);
	    Machine m = new Machine();
	    assertEquals(m.execute(list),7);
  }
  

}
